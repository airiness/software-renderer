#include "RandomGenerator.h"
using namespace Math;

RandomGenerator* RandomGenerator::pInstance = nullptr;

RandomGenerator::~RandomGenerator()
{
}

RandomGenerator& RandomGenerator::GetSinglton()
{
	if (!pInstance)
	{
		pInstance = new RandomGenerator();
	}

	return *pInstance;
}

void RandomGenerator::SetSeed(int seed)
{
	mt.seed(seed);
}

int RandomGenerator::RandInt(int from, int to)
{
	std::uniform_int_distribution<int> disti(from, to);
	return disti(mt);
}

float RandomGenerator::RandFloat(float from, float to)
{
	std::uniform_real_distribution<float> distf(from, to);
	return distf(mt);
}

double RandomGenerator::RandDouble(double from, float to)
{
	std::uniform_real_distribution<double> distd(from, to);
	return distd(mt);
}

RandomGenerator::RandomGenerator()
{
}
