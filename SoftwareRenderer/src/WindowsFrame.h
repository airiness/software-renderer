#pragma once
#include <Windows.h>
#include "WindowsConfiguration.h"

namespace SoftwareRenderer
{
	class WindowsFrame
	{
	public:
		WindowsFrame(const WindowsConfiguration& configuration);

		int Initialize();
		void Finalize();
		void Update();
		bool IsQuit();

		WindowsConfiguration GetConfiguration() const;
		HWND GetWindowHandle() const;

	private:
		static LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
	private:
		HWND m_hWnd;
		WindowsConfiguration m_Config;
		static bool m_bQuit;

	};

	extern WindowsFrame* g_pWindowsFrame;
}