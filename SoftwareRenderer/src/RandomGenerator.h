#pragma once
#include <random>

#define randi(f,t) Math::RandomGenerator::GetSinglton().RandInt(f,t)
#define randf(f,t) Math::RandomGenerator::GetSinglton().RandFloat(f,t)
#define randd(f,t) Math::RandomGenerator::GetSinglton().RandDouble(f,t)

namespace Math
{
	class RandomGenerator
	{
	public:
		~RandomGenerator();

		static RandomGenerator& GetSinglton();

		void SetSeed(int seed);

		int RandInt(int from, int to);
		float RandFloat(float from, float to);
		double RandDouble(double from, float to);

	private:
		RandomGenerator();
	private:
		static RandomGenerator* pInstance;
		std::mt19937 mt;
	};
}