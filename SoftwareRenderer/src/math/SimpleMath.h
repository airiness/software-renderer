#pragma once
#include <cassert>
#include <type_traits>

namespace Math
{
	template<typename T, int N>
	class Vector;

	template<typename T>
	struct Vector2Data
	{
		union
		{
			T data[2];
			T elem[2][1];
			struct { T x; T y; };
			struct { T r; T g; };
			struct { T u; T v; };
		};

		const T& operator[](int index) const
		{
			assert(index >= 0 && index < 2);
			return data[index];
		}

		T& operator[](int index)
		{
			assert(index >= 0 && index < 2);
			return data[index];
		}
	};

	template<typename T>
	struct Vector3Data
	{
		union
		{
			T data[3];
			T elem[3][1];
			struct { T x; T y; T z; };
			struct { T r; T g; T b; };
			struct { T u; T v; T s; };
		};


		const T& operator[](int index) const
		{
			assert((index >= 0 && index < 3) == true);
			return data[index];
		}

		T& operator[](int index)
		{
			assert((index >= 0 && index < 3) == true);
			return data[index];
		}
	};

	template<typename T>
	struct Vector4Data
	{
		union
		{
			T data[4];
			T elem[4][1];
			struct { T x; T y; T z; T w; };
			struct { T r; T g; T b; T a; };
			struct { T u; T v; T s; T t; };
		};

		const T& operator[](int index) const
		{
			assert(index >= 0 && index < 4);
			return data(index);
		}

		T& operator[](int index)
		{
			assert(index >= 0 && index < 4);
			return data(index);
		}
	};

	template<typename T, int N>
	class Vector : public std::conditional<N == 2, Vector2Data<T>,
		typename std::conditional<N == 3, Vector3Data<T>, 
		Vector4Data<T>>::type>::type
	{
	public:
		using BaseT = typename std::conditional<N == 2, Vector2Data<T>,
			typename std::conditional<N == 3, Vector3Data<T>, 
			Vector4Data<T>>::type>::type;

		using BaseT::data;
		using BaseT::elem;

		Vector() = default;

		Vector(T v0, T v1)
		{
			static_assert(N == 2, "");
			data[0] = v0;
			data[1] = v1;
		}

		Vector(T v0, T v1, T v2)
		{
			static_assert(N == 3, "");
			data[0] = v0;
			data[1] = v1;
			data[2] = v2;
		}

		Vector(T v0, T v1, T v2, T v3)
		{
			static_assert(N == 4, "");
			data[0] = v0;
			data[1] = v1;
			data[2] = v2;
			data[3] = v3;
		}

		Vector& operator=(const Vector& rhs) = default;
		Vector& operator=(T v)
		{
			for (int i = 0; i < N; ++i) { data[i] = v; }
			return *this;
		}



		void set(T v)
		{
			for (int i = 0; i < N; ++i) { data[i] = v; }
		}

	};

	template<typename T, int N>
	Vector<T, N> operator+(const Vector<T, N>& v0, const Vector<T, N>& v1)
	{
		Vector<T, N> result;
		for (int i = 0; i < N; ++i)
		{
			result[i] = v0[i] + v1[i];
		}
		return result;
	}

	template<typename T, int N>
	Vector<T, N> operator-(const Vector<T, N>& v0, const Vector<T, N>& v1)
	{
		Vector<T, N> result;
		for (int i = 0; i < N; ++i)
		{
			result[i] = v0[i] - v1[i];
		}
		return result;
	}

	template<typename T, int N>
	Vector<T, N> operator*(const Vector<T, N>& v, float n)
	{
		Vector<T, N> result;
		for (int i = 0; i < N; ++i)
		{
			result[i] = v[i] * n;
		}
		return result;
	}

	template<typename T, int N>
	Vector<T, N> operator*(float n, const Vector<T, N>& v)
	{
		Vector<T, N> result;
		for (int i = 0; i < N; ++i)
		{
			result[i] = v[i] * n;
		}
		return result;
	}


	using Vector2f = Vector<float, 2>;
	using Vector2i = Vector<int, 2>;
	using Vector3f = Vector<float, 3>;
	using Vector3i = Vector<int, 3>;
	using Vector4f = Vector<float, 4>;
	using Vector4i = Vector<int, 4>;
}

