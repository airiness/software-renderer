#include "Scene.h"

int SoftwareRenderer::Scene::Initialize()
{
	m_pObjects = std::make_unique<std::vector<Object*>>();
	return 0;
}

void SoftwareRenderer::Scene::Finalize()
{
	m_pObjects.reset();
}

void SoftwareRenderer::Scene::Update()
{
}

void SoftwareRenderer::Scene::SceneDraw()
{
}
