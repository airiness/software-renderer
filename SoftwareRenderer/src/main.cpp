#include <iostream>
#include "WindowsFrame.h"
#include "Renderer.h"

using namespace SoftwareRenderer;

int main(int argc, char* argv[])
{
	if (!g_pWindowsFrame->Initialize())
	{
		std::wcout << g_pWindowsFrame->GetConfiguration() << std::endl;
	}

	if (!g_pRenderer->Initialize())
	{
		std::wcout << _T("Render Intialized OK!") << std::endl;
	}

	while (!g_pWindowsFrame->IsQuit())
	{
		g_pWindowsFrame->Update();
		g_pRenderer->Update();

		g_pRenderer->ClearBuffer();
		g_pRenderer->RenderBuffer();
		g_pRenderer->SwapBuffer();
	}

	g_pRenderer->Finalize();
	g_pWindowsFrame->Finalize();
	return 0;
}