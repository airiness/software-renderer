#pragma once

namespace SoftwareRenderer
{
	class Game
	{
		int Initialize();
		void Update();
		void Finalize();

	};
}