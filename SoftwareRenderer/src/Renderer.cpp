#include <Windows.h>
#include <iostream>
#include <cassert>
#include <algorithm>
#include "Renderer.h"
#include "RandomGenerator.h"
#include "WindowsFrame.h"
#include "Keyboard.h"
using namespace SoftwareRenderer;
using namespace Input;
using namespace Math;

Renderer::Renderer()
	:m_screenWidth(0),
	m_screenHeight(0),
	m_channel(4),
	m_windowsHandle(nullptr),
	m_surface(nullptr),
	m_memoryDC(nullptr)
{

}

int Renderer::Initialize()
{
	int result = 0;
	m_windowsHandle = g_pWindowsFrame->GetWindowHandle();
	auto winconfig = g_pWindowsFrame->GetConfiguration();
	m_screenWidth = winconfig.screenWidth;
	m_screenHeight = winconfig.screenHeight;
	//m_buffer = std::make_unique<ScreenBuffer>(m_screenWidth, m_screenHeight);

	BITMAPINFOHEADER bmiHeader;
	HDC window_dc;
	HBITMAP dib_bitmap;
	HBITMAP old_bitmap;

	window_dc = GetDC(m_windowsHandle);
	m_memoryDC = CreateCompatibleDC(window_dc);
	ReleaseDC(m_windowsHandle, window_dc);

	ZeroMemory(&bmiHeader, sizeof(BITMAPINFOHEADER));
	bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmiHeader.biWidth = m_screenWidth;
	bmiHeader.biHeight = -m_screenHeight;
	bmiHeader.biPlanes = 1;
	bmiHeader.biBitCount = 32;
	bmiHeader.biCompression = BI_RGB;

	dib_bitmap = CreateDIBSection(m_memoryDC, (BITMAPINFO*)&bmiHeader, DIB_RGB_COLORS, (void**)&m_surface, nullptr, 0);

	assert(dib_bitmap != nullptr);
	old_bitmap = (HBITMAP)SelectObject(m_memoryDC, dib_bitmap);
	DeleteObject(old_bitmap);
	return result;
}

void Renderer::Finalize()
{
	//m_buffer.reset();
}

void Renderer::Update()
{
	auto kb = g_pKeyboard->GetState();
	g_pKeyboardStateTracker->Update(kb);
	if (kb.A)
	{
		std::cout << "a" << std::endl;
	}

	if (g_pKeyboardStateTracker->pressed.Left)
	{
		std::cout << "left" << std::endl;
	}
	if (g_pKeyboardStateTracker->pressed.Right)
	{
		std::cout << "right" << std::endl;
	}
}

void Renderer::ClearBuffer()
{
	std::fill(m_surface, m_surface + m_screenWidth * m_screenHeight * m_channel, 0xff);
}

void Renderer::RenderBuffer()
{

	//Vector3i cube_vertex1{-50,50,-50};
	//Vector3i cube_vertex2{50,50,-50};
	//Vector3i cube_vertex3{-50,50,50};
	//Vector3i cube_vertex4{50,50,50};
	//Vector3i cube_vertex5{-50,-50,50};
	//Vector3i cube_vertex6{50,-50,50};
	//Vector3i cube_vertex7{-50,-50,-50};
	//Vector3i cube_vertex8{50,-50,-50};

	static int x1 = 10, y1 = 20;
	static int x2 = 200, y2 = 500;
	Vector2i a{ x1,y1 };
	Vector2i b{ x2,y2 };

	auto kb = g_pKeyboard->GetState();
	g_pKeyboardStateTracker->Update(kb);
	if (kb.A)
	{
		std::cout << "a" << std::endl;
		x1--;
	}
	if (kb.D)
	{
		x1++;
	}
	if (kb.W)
	{
		y1--;
	}
	if (kb.S)
	{
		y1++;
	}

	x1 = (std::min)(m_screenWidth, x1);
	x1 = (std::max)(1, x1);
	y1 = (std::min)(m_screenHeight, y1);
	y1 = (std::max)(1, y1);

	//
	if (kb.Left)
	{
		std::cout << "a" << std::endl;
		x2--;
	}
	if (kb.Right)
	{
		x2++;
	}
	if (kb.Up)
	{
		y2--;
	}
	if (kb.Down)
	{
		y2++;
	}

	x2 = (std::min)(m_screenWidth, x2);
	x2 = (std::max)(1, x2);
	y2 = (std::min)(m_screenHeight, y2);
	y2 = (std::max)(1, y2);

	RendALine(a, b, { 1.0f,0.0f,0.0f,1.0f });
	//RendATriangle(cube_vertex1, cube_vertex2, cube_vertex3, {1.0f,0.0f,0.0f,1.0f});

	//size_t j = 0;
	//for (size_t i = 0; i < m_screenWidth * m_screenHeight * m_channel; i += m_channel)
	//{

	//	m_surface[i] = (*m_buffer->ColorBuffer)[j].b * 255.0f;
	//	m_surface[i + 1] = (*m_buffer->ColorBuffer)[j].g * 255.0f;
	//	m_surface[i + 2] = (*m_buffer->ColorBuffer)[j].r * 255.0f;
	//	m_surface[i + 3] = (*m_buffer->ColorBuffer)[j].a * 255.0f;
	//	j++;
	//}

	//for (size_t i = 0; i < m_screenWidth * m_screenHeight * m_channel; i++)
	//{
	//	m_surface[i] = 0xff;
	//}

}

void Renderer::SwapBuffer()
{
	HDC window_dc = GetDC(m_windowsHandle);
	BitBlt(window_dc, 0, 0, m_screenWidth, m_screenHeight, m_memoryDC, 0, 0, SRCCOPY);
	ReleaseDC(m_windowsHandle, window_dc);
}

void Renderer::RendALine(Vector2i from, Vector2i to, const Vector4f& color)
{
	bool steep = false;
	//if the line is steep?always use long side as for range
	if (std::abs(from.x - to.x) < std::abs(from.y - to.y))
	{
		std::swap(from.x, from.y);
		std::swap(to.x, to.y);
		steep = true;
	}
	//always from left to right
	if (from.x > to.x)
	{
		std::swap(from, to);
	}
	//optimize to get the d
	int dx = to.x - from.x;
	int dy = to.y - from.y;
	float derror2 = std::abs(dy) * 2;
	float error2 = 0;
	int y = from.y;
	for (int x = from.x; x < to.x; x++)
	{
		steep ? SetColor({ y,x }, color) : SetColor({ x,y }, color);
		error2 += derror2;
		if (error2 > dx)
		{
			y += (to.y > from.y ? 1 : -1);
			error2 -= dx * 2;
		}
	}
}

void Renderer::RendATriangle(Math::Vector2i v0, Math::Vector2i v1, Math::Vector2i v2, const Math::Vector4f& color)
{
	//sort the vertices by y
	if (v0.y > v1.y) std::swap(v0, v1);
	if (v0.y > v2.y) std::swap(v0, v2);
	if (v1.y > v2.y) std::swap(v1, v2);

	//RendALine(v0, v1, color);
	//RendALine(v1, v2, color);
	//RendALine(v2, v0, color);

	int total_height = v1.y - v0.y;
	int segment_height = v1.y - v0.y + 1;
	for (int y = v0.y; y < v1.y; y++)
	{
		float alpha = (float)(y - v0.y) / total_height;
		float beta = (float)(y - v0.y) / segment_height;
		Vector2i A = v0 + (v2 - v0) * alpha;
		Vector2i B = v0 + (v1 - v0) * beta;
		SetColor({ A.x,y }, color);
		SetColor({ B.x,y }, color);
	}
}

void SoftwareRenderer::Renderer::SetColor(const Math::Vector2i& position, const Math::Vector4f& color)
{
	int index = ((position.x - 1) + (position.y - 1) * m_screenWidth) * m_channel;
	m_surface[index] = color.b * 255.0f;
	m_surface[index + 1] = color.g * 255.0f;
	m_surface[index + 2] = color.r * 255.0f;
	m_surface[index + 3] = color.a * 255.0f;
}
