#pragma once
#include <cstdint>
#include <iostream>
#include <tchar.h>

namespace SoftwareRenderer
{
	struct WindowsConfiguration
	{
		WindowsConfiguration(int32_t width = 1920, int32_t height = 1080, const wchar_t* name = _T("BuNnsyuu"))
			:screenWidth(width), screenHeight(height), windowName(name)
		{

		}

		int32_t screenWidth;
		int32_t screenHeight;
		const wchar_t* windowName;


		friend std::wostream& operator<<(std::wostream& wout, const WindowsConfiguration& conf)
		{
			wout << "Software Renderer" << '\n'
				<< "Screen Width: " << conf.screenWidth << '\n'
				<< "Screen Height: " << conf.screenHeight << '\n'
				<< "Windows Name: " << conf.windowName << std::endl;
			return wout;
		}
	};
}
