#include "WindowsFrame.h"
#include "Renderer.h"
#include "Keyboard.h"
using namespace Input;

namespace SoftwareRenderer
{
	WindowsConfiguration configuration(800, 600, _T("BuNnsyuu"));
	WindowsFrame* g_pWindowsFrame = new WindowsFrame(configuration);
	Renderer* g_pRenderer = new Renderer();
	Keyboard* g_pKeyboard = new Keyboard();
	Keyboard::KeyboardStateTracker* g_pKeyboardStateTracker = new Keyboard::KeyboardStateTracker();
}