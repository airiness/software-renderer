#pragma once
#include <vector>
#include <memory>
#include "Object.h"

namespace SoftwareRenderer
{
	class Scene
	{
	public:

		int Initialize();
		void Finalize();
		void Update();

		void SceneDraw();
	private:
		std::unique_ptr<std::vector<Object*>> m_pObjects;
	};
}