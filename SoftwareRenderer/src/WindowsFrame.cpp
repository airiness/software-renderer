#include "WindowsFrame.h"
#include "Keyboard.h"
using namespace SoftwareRenderer;

bool WindowsFrame::m_bQuit = false;

WindowsFrame::WindowsFrame(const WindowsConfiguration& configuration)
	:m_Config(configuration), m_hWnd(nullptr)
{

}

int WindowsFrame::Initialize()
{
	HINSTANCE hInstance = GetModuleHandle(nullptr);

	WNDCLASSEXW wcex = { 0 };
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WindowProc;
	wcex.hInstance = hInstance;
	wcex.hCursor = LoadCursorW(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszClassName = _T("BuNnsyuu");

	if (!RegisterClassExW(&wcex))
	{
		return 1;
	}

	auto windowStyle = WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX;
	RECT rc = { 0,0,static_cast<LONG>(m_Config.screenWidth),static_cast<LONG>(m_Config.screenHeight) };
	AdjustWindowRect(&rc, windowStyle, FALSE);

	HWND hWnd = CreateWindowExW(0,
		_T("BuNnsyuu"),
		m_Config.windowName,
		windowStyle,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		rc.right - rc.left,
		rc.bottom - rc.top,
		nullptr,
		nullptr,
		hInstance,
		nullptr);

	ShowWindow(hWnd, SW_SHOWNORMAL);

	m_hWnd = hWnd;
	return 0;
}

void WindowsFrame::Finalize()
{
}

void WindowsFrame::Update()
{
	MSG msg;
	if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

bool WindowsFrame::IsQuit()
{
	return m_bQuit;
}

WindowsConfiguration WindowsFrame::GetConfiguration() const
{
	return m_Config;
}

HWND SoftwareRenderer::WindowsFrame::GetWindowHandle() const
{
	return m_hWnd;
}

LRESULT WindowsFrame::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_PAINT:
		break;
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		m_bQuit = true;
		return 0;
	}
	case WM_ACTIVATEAPP:
		Input::Keyboard::ProcessMessage(message, wParam, lParam);
		break;
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
	case WM_KEYUP:
	case WM_SYSKEYUP:
		Input::Keyboard::ProcessMessage(message, wParam, lParam);
		break;
	default:
		break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}
