#pragma once
#include "SimpleMath.h"

namespace SoftwareRenderer
{
	class Renderer
	{
	public:
		Renderer();

		int Initialize();
		void Finalize();
		void Update();

		void ClearBuffer();
		void RenderBuffer();
		void SwapBuffer();
	private:
		void RendALine(Math::Vector2i from,Math::Vector2i to,const Math::Vector4f& color);
		void RendATriangle(Math::Vector2i v0, Math::Vector2i v1, Math::Vector2i v2, const Math::Vector4f& color);

		void SetColor(const Math::Vector2i& position, const Math::Vector4f& color);
	private:
		int32_t m_screenWidth;
		int32_t m_screenHeight;
		uint32_t m_channel;

		HWND m_windowsHandle;
		unsigned char* m_surface;
		HDC m_memoryDC;

		//std::unique_ptr<ScreenBuffer> m_buffer;
	};

	extern Renderer* g_pRenderer;
}

